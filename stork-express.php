<?php
/**
     * @package Stork Express
     * @version 1.1.0
     */
    /*
    Plugin Name: Stork Express
    Plugin URI: https://stork.express/
    Description: Adds Carrier options operated by Stork Express. Configuration of working times and preparation time available in the module.
    Author: Stork SAS
    Version: 1.0.0
    Author URI: https://stork.express/
    */
     
    /*
      Copyright 2021  Stork SAS  (email : rumen@stork.express)
      This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.
      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
     */
     





/**
 * @internal never define functions inside callbacks.
 * these functions could be run multiple times; this would result in a fatal error.
 */

 // This enables debugging.
define( 'WP_DEBUG', true );


/**
 * 
 * custom option and settings
 */
function stork_express_settings_init() {
    // Register a new setting for "stork_express" page.
    register_setting( 'stork_express', 'stork_express_live_mode' );
    register_setting( 'stork_express', 'stork_express_email' );
    register_setting( 'stork_express', 'stork_express_working_hours_start_monday' );
   
 
    // Register a new section in the "stork_express" page.
    add_settings_section(
        'stork_express_section_developers',
        __( 'Stork Express settings.', 'stork_express' ), 'stork_express_section_developers_callback',
        'stork_express'
    );
 
    // Register a new field in the "stork_express_section_developers" section, inside the "stork_express" page.
    add_settings_field(
        'stork_express_live_mode', // As of WP 4.6 this value is used only internally.
                                // Use $args' label_for to populate the id inside the callback.
            __( 'Live mode', 'stork_express' ),
        'stork_express_live_mode_cb',
        'stork_express',
        'stork_express_section_developers',
        array(
            'label_for'         => 'stork_express_live_mode',
            'class'             => 'stork_express_row',
            'stork_express_custom_data' => 'custom',
        )
    );

    add_settings_field(
        'stork_express_working_hours_start_monday', 
                                
            __( 'Monday starts at :', 'stork_express' ),
        'stork_express_working_hours_start_monday_cb',
        'stork_express',
        'stork_express_section_developers',
        array(
            'label_for'         => 'stork_express_working_hours_start_monday',
            'class'             => 'stork_express_row',
            'stork_express_custom_data' => 'custom',
        )
    );
     
add_settings_field(
    'stork_express_email', 
                            
        __( 'Email', 'stork_express' ),
    'stork_express_email_cb',
    'stork_express',
    'stork_express_section_developers',
    array(
        'label_for'         => 'stork_express_email',
        'class'             => 'stork_express_row',
        'stork_express_custom_data' => 'custom',
    )
);

}
/**
 * Register our stork_express_settings_init to the admin_init action hook.
 */
add_action( 'admin_init', 'stork_express_settings_init' );
 
 
/**
 * Custom option and settings:
 *  - callback functions
 */
 
 
/**
 * Developers section callback function.
 *
 * @param array $args  The settings array, defining title, id, callback.
 */
function stork_express_section_developers_callback( $args ) {
    ?>
    <p class="description">
        <?php esc_html_e( 'Here is my new shipping plugin!
        I can configure it using the following configuration form.
        This plugin will boost your sales!', 'stork_express' ); ?></p>
    <?php
}
 
/**
 * Pill field callbakc function.
 *
 * WordPress has magic interaction with the following keys: label_for, class.
 * - the "label_for" key value is used for the "for" attribute of the <label>.
 * - the "class" key value is used for the "class" attribute of the <tr> containing the field.
 * Note: you can add custom key value pairs to be used inside your callbacks.
 *
 * @param array $args
 */
function stork_express_live_mode_cb() {
    // Get the value of the setting we've registered with register_setting()
    $option = get_option( 'stork_express_live_mode' );
    ?>
    <select name="stork_express_live_mode">
        <option value="1" <?php selected( $option,'1' ); ?>>
            <?php esc_html_e( 'yes', 'stork_express' ); ?>
        </option>
        <option value="0" <?php selected( $option,'0' ); ?>>
            <?php esc_html_e( 'no', 'stork_express' ); ?>
        </option>
    </select>
    <?php
} 

function stork_express_email_cb () {
    // Get the value of the setting we've registered with register_setting()
    $option = get_option( 'stork_express_email' );
    ?>
    <input type="text" name='stork_express_email' value=" <?php echo $option; ?>">
<?php
}

function stork_express_working_hours_start_monday_cb () {
    $option = get_option( 'stork_express_working_hours_start_monday' );
    ?>
    <input type="text" name='stork_express_working_hours_start_monday' value=" <?php echo $option; ?>">
<?php
}

/**
 * Add the top level menu page.
 */
function stork_express_options_page() {
    add_menu_page(
        'stork_express',
        'Stork Express Options',
        'manage_options',
        'stork_express',
        'stork_express_options_page_html'
    );
}
 
 
/**
 * Register our stork_express_options_page to the admin_menu action hook.
 */
add_action( 'admin_menu', 'stork_express_options_page' );
 
 
/**
 * Top level menu callback function
 */
function stork_express_options_page_html() {
    // check user capabilities
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }
 
    // add error/update messages
 
    // check if the user have submitted the settings
    // WordPress will add the "settings-updated" $_GET parameter to the url
    if ( isset( $_GET['settings-updated'] ) ) {
        // add settings saved message with the class of "updated"
        add_settings_error( 'stork_express_messages', 'stork_express_message', __( 'Settings Saved', 'stork_express' ), 'updated' );
    }
 
    // show error/update messages
    settings_errors( 'stork_express_messages' );
    ?>
    <div class="wrap">
        <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
        <form action="options.php" method="post">
            <?php
            // output security fields for the registered setting "stork_express"
            settings_fields( 'stork_express' );
            // output setting sections and their fields
            // (sections are registered for "stork_express", each field is registered to a specific section)
            do_settings_sections( 'stork_express' );
            // output save settings button
            submit_button( 'Save Settings' );
            ?>
        </form>
    </div>
    <?php
} 